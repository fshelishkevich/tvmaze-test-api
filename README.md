#NOTE
App code placed here `/src/pages/Index.vue`

# Quasar App (tvmaze)
## Install Quasar
```bash
$ yarn global add @quasar/cli
# or
$ npm install -g @quasar/cli
```
https://quasar.dev/quasar-cli/installation


## To run app
Install the dependencies
```bash
yarn
```
Run app
```bash
quasar dev
```
